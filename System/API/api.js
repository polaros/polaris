const ENVIRONMENT_VARIABLES = require("../env/environment_variables");
const LOGGER = require("../root/logger");
const SYSTOOLS = require("../root/systools");
const USERS = require("../root/users");
const CUSTOMIZATION = require("../root/customization");
const DB = require("../root/db");
const SCREEN = require("../screen/screen")
const CRASH_HANDLER = require("../modules/CrashHandler/CrashHandler")
const POLARCOMMANDMANAGER = require("../modules/PolarCommandManager/PolarCommandManager")
const IPC = require("electron").ipcRenderer


exports.ENVIRONMENT_VARIABLES = ENVIRONMENT_VARIABLES;
exports.LOGGER = LOGGER;
exports.SYSTOOLS = SYSTOOLS;
exports.USERS = USERS;
exports.CUSTOMIZATION = CUSTOMIZATION;
exports.DB = DB;
exports.SCREEN = SCREEN;
exports.CRASH_HANDLER = CRASH_HANDLER;
exports.POLARCOMMANDMANAGER = POLARCOMMANDMANAGER;
