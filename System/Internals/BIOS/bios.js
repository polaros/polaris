const API = require("../../API/api")
const all_envs = API.ENVIRONMENT_VARIABLES.load_static()


let welcome_messages = [
    "Screwed Up Stuff?",
    "Oh hi again!",
    "PolarOS, here to fix stuff?",
    "Perhaps try resetting?",
    "Hmmm, any clue?",
    "Hello there!",
    "Hello!",
    "Reconsider?",
    "Fixy Fixy", 
    "These Quotes, just to help",
    "0.1 + 0.2 = 0.30000000000000004"
]

document.getElementById("header").innerText = welcome_messages[Math.floor(Math.random() * welcome_messages.length)];


for (let x in all_envs) {
    let value = all_envs[x].value
    let tilehtml = `
    <div class="tile">
        <center class="tilecenter">
            <p class="varname" id="varname-${x}">
                ${all_envs[x].name}
            </p>
            <p>${value}</p>
            <input class="varinput" value="${value}" id="${x}">
            <button class="savebtn" onclick="savevar(${x})" style="margin-right: 20px;"><i class="fas fa-save"></i></button>
        </center>
    </div>
`
    if (typeof(value) == "object") {
        value = JSON.stringify(all_envs[x].value).toString()
        tilehtml = `
    <div class="tile">
        <center class="tilecenter">
            <p class="varname" id="varname-${x}">
                ${all_envs[x].name}
            </p>
            <p style="font-size: 16px; margin-right: 20px">${value}</p>
            <button class="savebtn" onclick="savevar(${x})" style="margin-right: 20px;"><i class="fas fa-save"></i></button>
        </center>
    </div>
`
    } else {
        value = value.toString()
        tilehtml = `
    <div class="tile">
        <center class="tilecenter">
            <p class="varname" id="varname-${x}">
                ${all_envs[x].name}
            </p>
            <input class="varinput" value="${value}" id="${x}">
            <button class="savebtn" onclick="savevar(${x})" style="margin-right: 20px;"><i class="fas fa-save"></i></button>
        </center>
    </div>
`
    }
    document.getElementById("optionsmenu").innerHTML += tilehtml
}

window.onkeydown = (e) => {
    if (e.key == "Escape") {
        bios_exit()
        setTimeout(() => {
            window.open("../../screen/index.html", "_self")
        }, 3000);
        return
    }
}

function savevar(id) {
    let inputfield = document.getElementById(id)
    let varname = document.getElementById("varname-" + id)
    API.ENVIRONMENT_VARIABLES.set_static(varname.innerText, inputfield.value)
}

function bios_exit() {
    let loader = `
    <div class="preloader loading">
      <span class="slice"></span>
      <span class="slice"></span>
      <span class="slice"></span>
      <span class="slice"></span>
      <span class="slice"></span>
      <span class="slice"></span>
    </div>
    `
    document.getElementById("center").innerHTML = loader
}