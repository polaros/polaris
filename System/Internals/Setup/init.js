const api = require("api");
api.CUSTOMIZATION.reset();
api.SYSTOOLS.copyFileSync(__dirname + "/default-bg/default.jpg", "wallpapers/default.jpg", true);
document.body.style.backgroundColor = "black";
document.body.style.backgroundImage = "";
const checks = [
    () => {
        document.getElementById("header-container").innerHTML = `
            <p>We are Setting up things for first boot.</p>
            <div class="lds-dual-ring"></div>
        `
    },
    ()=> {
        setTimeout(()=> {
            document.body.classList.add("fadeOut");
            setTimeout(()=>{
                window.open("../firstboot/index.html", "_self")
            }, 600)
        }, 2000)
    }
]

checks.forEach(c=>c())