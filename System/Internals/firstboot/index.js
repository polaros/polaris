const API = require("api")

let SetupInfo = {
    step: 0
}

addFadeIn("bodyID")



function addFadeOut(id) {
    document.getElementById(id).classList.remove("fadeIn")
    document.getElementById(id).classList.add("fadeOut");
}

function addFadeIn(id) {
    document.getElementById(id).classList.remove("fadeOut")
    document.getElementById(id).classList.add("fadeIn");
}

function changeSystemMessage(text) {
    document.getElementById("SystemMessage").innerText = text;
}

function changeOperationMessage(text) {
    document.getElementById("SystemMsg").innerHTML = text;
}

function changeOperationIcon(html) {
    document.getElementById("SysActionIcon").innerHTML = html;
}

function changeOperationMsg(text) {
    document.getElementById("OperationMsg").innerHTML = text;
}

function changeOperationMisc(html) {
    document.getElementById("operationMisc").innerHTML = html;
}


function updateProgress() {
    document.getElementById("progressBar").innerHTML = Math.round((SetupInfo.step / SetupFunctions.length) * 100) + "%"
}



function HandleInput() {
    if (SetupInfo.step < SetupFunctions.length) {
        let functionResult = SetupFunctions[SetupInfo.step]();
        SetupInfo.step++;
        updateProgress()
    }
}

setTimeout(()=>{
    changeSystemMessage("Enabling Mouse Input")
},1000)
setTimeout(()=> {changeSystemMessage("FirstBoot Screen")},2000)

function Quit() {
    window.close()
}

let SetupFunctions = [
    () => {
        let InputVal = document.getElementById("SysInput").value
        if (InputVal.split(" ").join("") != "") {
            SetupInfo["username"] = InputVal;
            addFadeOut("SystemMsg");
            setTimeout(()=>{
                changeOperationMessage(`Hello ${InputVal}`)
                addFadeIn("SystemMsg")
                setTimeout(()=> {
                    addFadeOut("SystemMsg")
                    setTimeout(()=>{
                        changeOperationMessage("Would you like to add a password?");
                        addFadeIn("SystemMsg");
                        HandleInput()
                    }, 600)
                },2000)
            },600)
        }
    },
    () => {
        changeOperationIcon(`<i class="fas fa-key"></i>`)
        changeOperationMsg("Enter a password")
        document.getElementById("SysInput").value = ""
        document.getElementById("SysInput").type = "password"
    },
    ()=>{
        inputVal = document.getElementById("SysInput").value;
        if (inputVal.split(" ").join("") != "") {
            SetupInfo["password"] = API.SYSTOOLS.hasher(inputVal)
        } else {
            SetupInfo["password"] = null   
        }
        addFadeOut("SystemMsg")
        setTimeout(()=>{
            changeOperationMessage("Creating User...")
            API.USERS.createUser(SetupInfo.username, SetupInfo.password, 2)
            API.DB.table("global-info").set("CurrentUser", {
                username: SetupInfo.username,
                userID: 0
            })
            addFadeIn("SystemMsg")
            setTimeout(()=>{
                addFadeOut("SystemMsg")
                setTimeout(()=>{
                    changeOperationMessage('<i class="fas fa-check"></i>')
                    addFadeIn("SystemMsg")
                    changeSystemMessage(new Date().toDateString())
                    setTimeout(()=>{
                        addFadeOut("SystemMsg")
                        setTimeout(()=>{
                            addFadeOut("SystemMsg");
                            addFadeOut("operationMisc")
                            addFadeOut("SysActionIcon")
                            addFadeOut("SubmitGo")
                            setTimeout(()=>{
                                HandleInput();
                            }, 600)
                        })
                    }, 600)
                }, 600)

            }, 600)
        },600)
    },
    ()=>{
        changeOperationMsg("Theme Selection:")
        changeOperationMessage("Choose your preference")
        changeOperationIcon(`<i class="fas fa-eye"></i>`)
        changeOperationMisc(`
        <div class="optionTile" style="background-color: black" onclick="ThemeSelection('dark')">
            <center style="align-items: center; width: 100%;">
                <p style="text-align: left; padding-left: 10px; color: white" id="darkmode">
                    Dark Mode <i class="fas fa-check-square" id="selectiondark"></i>
                </p>
            </center>
        </div>
        <div class="optionTile" style="background-color: white; border: solid 2px black" onclick="ThemeSelection('light')">
            <center style="align-items: center; width: 100%;">
                <p style="text-align: left; padding-left: 10px; color: black" id="lightmode">
                    Light Mode
                </p>
            </center>
        </div>
        
        `)
        //document.getElementsByClassName("optionTile")[0].setAttribute('onclick', "ThemeSelection('dark')")
        setTimeout(()=>{
            addFadeIn("SystemMsg");
            addFadeIn("operationMisc")
            addFadeIn("SysActionIcon")
            addFadeIn("SubmitGo")

        },600)
        
    }
]

function checkUpdates() {
    document.getElementById("fixedContainer").innerHTML += `<div class="updateContainer" id="updateContainer">
        <center style="width: 100%; align-items: center; display:flex">
        <i class="fas fa-download"></i>
        <p id="updateText" style="margin-left: 2px">Checking For Updates</p>
        </center>
    </div>`
    setTimeout(()=>{
        addFadeOut("updateText")
        setTimeout(()=>{
            document.getElementById("updateText").innerHTML = "No Updates Available"
            addFadeIn("updateText")
            setTimeout(()=>{
                addFadeOut("updateContainer")
                setTimeout(()=>{document.getElementById("updateContainer").remove()},2000)
            }, 2000)
        }, 600)
    }, 2000)

}


function ThemeSelection(val) {
    SetupInfo["theme"] = val
    if (val === "light") {
        if (document.getElementById('selectiondark') === undefined) {return;}
        document.getElementById(`selectiondark`).remove()
        document.getElementById("lightmode").innerHTML += `<i class="fas fa-check-square" id="selectionlight"></i>`
    }
    if (val === "dark") {
        if (document.getElementById('selectionlight') === undefined) {return;}
        document.getElementById(`selectionlight`).remove()
        document.getElementById("darkmode").innerHTML += `<i class="fas fa-check-square" id="selectiondark"></i>`
    }
}

checkUpdates()