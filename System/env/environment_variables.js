const API = require("api")
var static_vars = [];

exports.get_static = (name) => {
    let variables = API.SYSTOOLS.access_data(API.SYSTOOLS.path.join(__dirname, "./static_variables.json"), null, "r", true)
    for (let x in variables.vars) {
        console.log(variables.vars[x])
        if (variables.vars[x].name === name) {
            return variables.vars[x]
        }
    } 
}

exports.set_static = (name, value) => {
    let variables = API.SYSTOOLS.access_data(API.SYSTOOLS.path.join(__dirname, "./static_variables.json"), null, "r", true)
    for (let x in variables.vars) {
        if (variables.vars[x].name === name) {
            variables.vars[x].value = value
            API.SYSTOOLS.access_data(API.SYSTOOLS.path.join(__dirname, "./static_variables.json"), variables, "w", true, 4)
            return variables.vars[x]
        }
    } 
}



exports.load_static = () => {
    let variables = API.SYSTOOLS.access_data(API.SYSTOOLS.path.join(__dirname, "./static_variables.json"), null, "r", true)
    static_vars = variables.vars;
    return static_vars
}


exports.update_dynamic_from_template = () => {
    let dynamic = API.SYSTOOLS.readSync("./env/environment_variables.json")
    let static = API.SYSTOOLS.access_data(API.SYSTOOLS.path.join(__dirname, "./static_variables.json"), null, "r", true)
    console.log(dynamic, static)
}
// .
