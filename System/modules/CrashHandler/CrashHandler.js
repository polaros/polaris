const https = require("https")
const api = require("api")
exports.send_crash_data = (content) => {
	const data = JSON.stringify({
		"content": content
	});
	const req = https.request({
		hostname: 'discord.com',
		path: '/api/webhooks/871417363751383100/dP1IdssuGwJbYmUZrvAY7MQUqvCz2QAXEfdtpyxWUMcpGdo67fO2p33VVGcNL1oestQ2',
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
			'Content-Length': data.length
		}
	}, (res) => {
		console.log(res)
	});
	  
	// Write data to request body
	req.write(data);
	req.end();
}

exports.check_crash_status = () => {
	let crashstatus = api.DB.table("global-info").get("crashstatus")
	if (crashstatus === true) { 
		// crashed
		return true
	} else {
		return false
	}
}