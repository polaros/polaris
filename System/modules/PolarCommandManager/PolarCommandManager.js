class PolarCommandManager {
    constructor () {
        this.COMMANDS = [];
    }

    addCommand(prefix, CommandName) {
        let commandID = this.COMMANDS.length
        let construct = {
            CommandPrefix: prefix,
            CommandName: CommandName,
            commandID: commandID
        }
        this.COMMANDS.push(construct)
        return commandID
    }

    addArg(commandID,argName,  fn) {
        this.COMMANDS[commandID][argName.toString()] = fn
        return this.COMMANDS[commandID];
    }

    parse(string) {
        for (let x in this.COMMANDS) {
            let command = this.COMMANDS[x];
            let commandPrefix = command.CommandPrefix;
            let commandName = command.CommandName;
            let commandFirstArgs = commandPrefix + commandName
            if (commandName === string.slice(commandPrefix.length, commandName.length + 1)) {
                let splitStr = string.slice(commandFirstArgs.slice(commandFirstArgs.length, string.length).length, string.length).split(/ /).slice(1)
                if (command[splitStr[0]] != undefined) {
                    command[splitStr[0]]();
                }
            }
        }
    }
}


exports.POLARCOMMANDMANAGER = PolarCommandManager;