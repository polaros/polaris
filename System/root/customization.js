const api = require("api");
const sass = require("sass");


exports.reset = () => {
  api.SYSTOOLS.writeSync(
    "scss/custom/color.scss",
    `
        $primary: #8926C7;
        $text: white;
        $background: black
    `
  );
  api.SYSTOOLS.writeSync(
    "scss/custom/background.scss",
    `
    $backgroundImg: "` + api.SYSTOOLS.get_relative_path("wallpapers/active.jpg").replaceAll("\\", "/") + `";
    $backgroundImgDefault: "` + api.SYSTOOLS.get_relative_path("wallpapers/default.jpg").replaceAll("\\", "/") + `"
    `
  );
  api.SYSTOOLS.copyFileSync(api.DB.table("global-info").get("root") + "/System/stylesheets/styles.scss", "scss/all.scss", true);
  this.compile();
};


var i = 0;
exports.compile = () => {
  api.SYSTOOLS.writeSync(
    "scss/result.css",
    sass.compile(api.SYSTOOLS.get_relative_path("scss/all.scss")).css
  );
  this.loadStyle();
  i++
};

exports.loadStyle = () => {
    if(document.getElementById("customCss") == null){
        var elem = document.createElement("style");
        elem.id = "customCss";
        document.head.append(elem);
    }else{
        var elem = document.getElementById("customCss");
    }
    elem.innerHTML = api.SYSTOOLS.readSync("scss/result.css");
    
}

exports.updateColors = (primary, text, background) => {
    api.SYSTOOLS.writeSync(
      "scss/custom/color.scss",
      `
          $primary: ` + primary + `;
          $text: ` + text + `;
          $background: ` + background + `
      `
    );
};

exports.updateBackground = (path, pathFull) => {
  api.SYSTOOLS.copyFileSync(path, "wallpapers/active.jpg", pathFull);
  api.SYSTOOLS.writeSync(
    "scss/custom/background.scss",
    `
        $backgroundImg: "` + api.SYSTOOLS.get_relative_path("wallpapers/active.jpg").replaceAll("\\", "/") + `?i=` + i + `";
        $backgroundImgDefault: "` + api.SYSTOOLS.get_relative_path("wallpapers/default.jpg").replaceAll("\\", "/") + `";
    `
  );
}
