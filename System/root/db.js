const JSONdb = require('simple-json-db');
const api = require("api");


exports.table = (name) => {
    return new JSONdb(api.SYSTOOLS.get_relative_path("db/" + name + ".json"));
}