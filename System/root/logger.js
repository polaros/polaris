const API = require("api")

exports.get_log_count = () => {
    let returnl = [];
    let files = null
    if (API.SYSTOOLS.fs.existsSync(API.SYSTOOLS.path.join(__dirname, "../logs"))) { 
        files = API.SYSTOOLS.fs.readdirSync(API.SYSTOOLS.path.join(__dirname, "../logs/"));

    } else {
        API.SYSTOOLS.fs.mkdirSync(API.SYSTOOLS.path.join(__dirname, "../logs"))
        files = API.SYSTOOLS.fs.readdirSync(API.SYSTOOLS.path.join(__dirname, "../logs/"));

    }
    files.forEach(f=> {if (f.name!='logger.js'){returnl.push(f)}})
    return returnl;
}

exports.delete_logs = () => {
    let files = this.get_log_count();
    files.forEach(f=>{
        API.SYSTOOLS.fs.unlinkSync(API.SYSTOOLS.path.join(__dirname, `../logs/${f}`))
    })
}

exports.get_last_log = () => {
    if (API.SYSTOOLS.fs.existsSync(API.SYSTOOLS.path.join(__dirname, "../logs"))) { 
        return API.SYSTOOLS.access_data(API.SYSTOOLS.path.join(__dirname, `../logs/${this.get_log_count().length - 1}.polar`), null, "r", false);
    } else {
        API.SYSTOOLS.fs.mkdirSync(API.SYSTOOLS.path.join(__dirname, "../logs"))
        return API.SYSTOOLS.access_data(API.SYSTOOLS.path.join(__dirname, `../logs/${this.get_log_count().length - 1}.polar`), null, "r", false);
    }
    
}

exports.add_boot_log = (log) => {
    let log_count = this.get_log_count()
    let log_str = `
        ==============================
        PolarOS Log #${log_count.length}
        Log Date ${new Date().toDateString()}
        System ${log.vitals}
        Boot State ${log.state}
        BootMng Ver. ${log.bootver}
        PolarOS Ver. ${log.polarver}
        ==============================
        ${log.content}

    `
    if (API.SYSTOOLS.fs.existsSync(API.SYSTOOLS.path.join(__dirname, "../logs")) === false) {
        API.SYSTOOLS.fs.mkdirSync(API.SYSTOOLS.path.join(__dirname, "../logs"))
    }
    API.SYSTOOLS.access_data(API.SYSTOOLS.path.join(__dirname, `../logs/${log_count.length}.polar`), log_str, "w", false)

}