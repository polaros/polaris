const fs = require("fs");
const path = require("path");
const API = require("../API/api");
exports.fs = fs;
exports.path = path;

exports.access_data = (
  file,
  data = null,
  state,
  jsonmode = true,
  indent = 4
) => {
  if (state === "r") {
    if (jsonmode === true) {
      return JSON.parse(fs.readFileSync(file));
    }
    return fs.readFileSync(file);
  } else if (state === "w") {
    if (jsonmode === true) {
      fs.writeFileSync(file, JSON.stringify(data, null, indent));
      return;
    }
    fs.writeFileSync(file, data);
    return;
  }
};

exports.hasher = (string) => {
  var hash = 0;

  if (string.length == 0) return hash;

  for (i = 0; i < string.length; i++) {
    char = string.charCodeAt(i);
    hash = (hash << 5) - hash + char;
    hash = hash & hash;
  }

  return hash;
};

exports.get_relative_path = (InputPath) => {
  const basePath = API.ENVIRONMENT_VARIABLES.get_static("PATH").value.BasePath;
  if (!fs.existsSync(path.dirname(path.join(basePath, InputPath)))) {
    fs.mkdirSync(path.dirname(path.join(basePath, InputPath)), {
      recursive: true,
    });
  }
  return path.join(basePath, InputPath);
};

exports.writeToRelativePath = (
  file,
  data = null,
  state,
  jsonmode = true,
  indent = 4
) => {
  if (state === "r") {
    if (jsonmode === true) {
      return JSON.parse(fs.readFileSync(this.get_relative_path(file)));
    }
    return fs.readFileSync(this.get_relative_path(file));
  } else if (state === "w") {
    if (jsonmode === true) {
      fs.writeFileSync(
        this.get_relative_path(file),
        JSON.stringify(data, null, indent)
      );
      return;
    }
    fs.writeFileSync(this.get_relative_path(file), data);
    return;
  }
};

exports.writeSync = (file, data) => {
  return fs.writeFileSync(this.get_relative_path(file), data);
};
exports.readSync = (file) => {
  if (!fs.existsSync(this.get_relative_path(file))) {
    return null;
  }
  return fs.readFileSync(this.get_relative_path(file));
};
exports.fileExists = (file) => {
  return fs.existsSync(this.get_relative_path(file));
};
exports.copyFileSync = (path, destination, pathFull = false) => {
  if(pathFull){
    return fs.copyFileSync(path,this.get_relative_path(destination))
  }
  return fs.copyFileSync(this.get_relative_path(path),this.get_relative_path(destination))
}

exports.mkdirSync = (file) => {
	fs.mkdirSync(this.get_relative_path(file))
}