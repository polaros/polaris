const API = require("api");

exports.createUser = (username, password, access_level = 0) => {
    if (API.SYSTOOLS.fileExists("./Users")) {
        if (API.SYSTOOLS.fileExists("./Users/" + username)) {
            return false;
        }
        API.SYSTOOLS.mkdirSync("./Users/" + username);
        API.DB.table("users").set(username, {
            username: username,
            password: password,
            createdAt: new Date(),
            access_level: access_level,
            folder: API.SYSTOOLS.get_relative_path("./Users/" + username),
            userID: Object.keys(API.DB.table("users").storage).length
        })
    } else {
        API.SYSTOOLS.mkdirSync("./Users")
        this.createUser(username, password, access_level);
    }
}