let bootstate = "";
let consolelogs = [];
let continueAllowed = false;
const API = require("api");
const path = require("path");

API.DB.table("global-info").set("root", path.join(__dirname, "../../"));
let env_path = API.ENVIRONMENT_VARIABLES.get_static("PATH")
env_path.value.Root = API.SYSTOOLS.path.join(__dirname, "../../")
API.ENVIRONMENT_VARIABLES.set_static("PATH", env_path.value)

function consoleprint(text) {
  consolelogs.push({
    text: text,
    printedAt: new Date(),
    calledBy: consoleprint.caller,
    id: consolelogs.length + 1,
  });
  document.getElementById("systextcontainer").innerHTML = `<div id="${
    consolelogs.length + 1
  }">
            ${text}
        </div>`;
  return consolelogs.length + 1;
}

function consoledelete(id) {
  document.getElementById(id.toString()).remove();
}

let checks = [
  () => {
    consoleprint("Loading Environment Variables");
    API.ENVIRONMENT_VARIABLES.load_static();
  },
  () => {
    consoleprint(
      `PolarOS Ver. ${
       API.ENVIRONMENT_VARIABLES.get_static("polarver").value
      }`
    );
  },
  () => {
    consoleprint("Checking for BIOS upgrades");
    // to be implemented
  },
  () => {
    consoleprint("Checking System Integrity");
    // to be implemented
  },
  () => {
    consoleprint("Checking for Crashes...");
    const crash_handler = API.CRASH_HANDLER;
    if (crash_handler.check_crash_status() === true) {
		window.open("../Internals/CrashScreen/index.html", "_self");
    } else {
		API.DB.table("global-info").set("crashstatus", true);
		//bootstate += "\nNo Previous Crashes"
      //crash_handler.send_crash_data("PolarOS found a crash from the previous instance, Please Standby\nCollecting Data...")
    }
  },
  () => {
    consoleprint("Logging Details");
    bootstate += "\n\n\nSystemLog:\n\nBooting";
    consolelogs.forEach((l) => (bootstate += "\n" + l.text));
    const env = API.ENVIRONMENT_VARIABLES;
    env.load_static();
    API.LOGGER.add_boot_log({
      vitals: "All Good.",
      state: "All Good.",
      bootver: env.get_static("bootver").value,
      polarver: env.get_static("polarver").value,
      content: bootstate,
    });
  },
  () => {
    console.log("Getting Things Ready")
  },
  () => {
    consoleprint("Loading Into PolarOS");
    continueAllowed = true;
  },
];

checks.forEach((fn) => fn());

var continueCheck = setInterval(() => {
  if (continueAllowed) {
    clearInterval(continueAllowed);
    window.open("../Internals/Splash/init.html", "_self");
    /*let systools = API.SYSTOOLS;
    let new_screen = API.SCREEN.change_screen(
      systools.path.join(__dirname, "../Internals/Splash/init.html"),
      systools.path.join(__dirname, "../Internals/Splash/init.js")
    );
    document.documentElement.innerHTML = new_screen.html;
    let js = document.createElement("script");
    let css = document.createElement("style");
    js.appendChild(document.createTextNode(new_screen.js));
    css.appendChild(document.createTextNode(new_screen.css));
    document.head.appendChild(js);
    document.head.appendChild(css);
    //require("./screen.js").change_screen(require("../root/systools").path.join(__dirname, "./index.html"), require("../root/systools").path.join(__dirname, "../Internals/Login/login.html"))
    */
  }
}, 2000);


process.on('uncaughtException', (error)=>{
  window.open("../Internals/BIOS/bios.html")
})