const systools = require("../root/systools.js")
exports.change_screen = (filepath, jspath=null, csspath=null) => {
    if (jspath != null) {
        jspath = systools.access_data(jspath, null, "r", false, 0) 
    }
    if (csspath != null) {
        csspath = systools.access_data(csspath, null, "r", false, 0) 
    }
    return {
        "html": systools.access_data(filepath, null, "r", false, 0),
        "js": jspath,
        "css": csspath
    }
}

// change_screen is now deprecated