const {app, BrowserWindow, crashReporter} = require("electron");
const path = require("path");
function init() {
    let mainWindow =  new BrowserWindow({
        "minHeight": 600,
        "minWidth": 800,
        "autoHideMenuBar": true,
        "alwaysOnTop": true,
        "webPreferences" : {
            nodeIntegration: true,
            contextIsolation: false,
            webviewTag: true,
            nodeIntegrationInWorker: true,
            nodeIntegrationInSubFrames: true,
            preload: path.join(__dirname, "./System/API/api.js")
        },
        backgroundColor: "#000000",
        fullscreen: true
    })
    mainWindow.loadFile(path.join(__dirname, "/System/screen/index.html"))

    mainWindow.webContents.on("crashed", (e)=>{
        app.relaunch();
        app.quit();
    })

    mainWindow.webContents.on("did-fail-load", (e)=>{
        //
    })
}
app.on('ready', init);

app.on('activate', ()=>{
    if (electron.BrowserWindow.getAllWindows().length === 0) {
        crashReporter.start({submitURL: "https://discord.com/api/webhooks/871417363751383100/dP1IdssuGwJbYmUZrvAY7MQUqvCz2QAXEfdtpyxWUMcpGdo67fO2p33VVGcNL1oestQ2"})
        init();
    }
})